#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#define CONST 10

char  globVar;

int main()
{
    int locVar;
    int * myTab;
    char caract ;
    
    myTab=(int*) malloc(CONST*sizeof(int));
    printf("Donner un mot : ");
    
    printf("PID : %d \n", getpid());
    printf("Adresses des variables : \n Adresse LocalVar : %p \n Adresse de globVar : %p \n Adresse de myTab : %p  \n Adresse CONST : %p \n",locVar,globVar,myTab,CONST);
    
    printf("Adressedes fonctions : \n Adresse fonction main : %p \n Adresse fonction malloc %p\n", main, malloc);
    
    printf("Saisissez un caractère \n");
    scanf(&caract);
    
    return 0;
}

// Question : suite à la commande readelf -s
/* Seule la variable globVar qui est une variable globale, non dépendante du contextye d'éxécution est présente dans le résultat de cette commande puisque les autres variables seront crées uniquement lors de l'exécution de notre exécutable
Et la constante n'a pas d'adresse , c'est pour cela que l'on ne la retrouve pas non plus ici 
Ensuite, on retrouve également les adresses fonctions main et malloc puisqu'elle existent , elles ne sont pas dépendantes du contexte d'execution */

